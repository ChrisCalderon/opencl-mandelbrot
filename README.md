# opencl-mandelbrot

This is a python script that renders a Mandelbrot zoom.
The fractal is rendered on the GPU using an OpenCL kernel and PyOpenCL for the glue.
The pixels are then piped to ffmpeg to render the video using libx265.