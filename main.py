# opencl-mandelbrot
# Copyright (C) 2023  Christian Calderon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import pyopencl as cl
from time import perf_counter
import subprocess
import os

os.environ['PYOPENCL_COMPILER_OUTPUT'] = '1'

WIDTH = 1920
HEIGHT = 1080
FRAMES = 1800
MS = 5
ZOOM = 0.99 # each frame zooms in by 1%
INITIAL_REAL_WIDTH = 4.0
INITIAL_IMAG_HEIGHT = INITIAL_REAL_WIDTH * HEIGHT / WIDTH
MAX_ITERS = 3500
CENTER_REAL = -0.753610741990141491504
CENTER_IMAG = +0.048446054249806423574
GAMMA = 2
# I stole this color palette from:
# http://www.pygame.org/project-Mandelbrot+Set+Viewer-698-.html
PALETTE_SRGB = [
    (25, 7, 26, 0),
    (9, 1, 47, 0),
    (4, 4, 73, 0),
    (0, 7, 100, 0),
    (12, 44, 138, 0),
    (24, 82, 177, 0),
    (57, 125, 209, 0),
    (134, 181, 229, 0),
    (211, 236, 248, 0),
    (241, 233, 191, 0),
    (248, 201, 95, 0),
    (255, 170, 0, 0),
    (204, 108, 0, 0),
    (153, 87, 0, 0),
    (106, 52, 3, 0),
    (66, 30, 15, 0)
]
PALETTE_LINEAR = np.array(PALETTE_SRGB, dtype=cl.cltypes.double3)
PALETTE_LINEAR.dtype = np.float64
#PALETTE_LINEAR /= 255.0
PALETTE_LINEAR **= GAMMA
PALETTE_LINEAR.dtype = cl.cltypes.double3
KERNEL = f"""
#define MS {MS}
#define MAX_ITERS {MAX_ITERS}
#define CENTER_REAL {CENTER_REAL}
#define CENTER_IMAG {CENTER_IMAG}
#define INITIAL_REAL_WIDTH {INITIAL_REAL_WIDTH}
#define INITIAL_IMAG_HEIGHT {INITIAL_IMAG_HEIGHT}
#define WIDTH {WIDTH}
#define HEIGHT {HEIGHT}
#define PALETTE_LEN {len(PALETTE_SRGB)}
//#define GAMMA_INV {1.0/GAMMA}
#define ZOOM {ZOOM}

__kernel void mandelbrot(__global const double3 *palette,
                         write_only image3d_t result)
{{
  int col = get_global_id(0);
  int row = get_global_id(1);
  int frame = get_global_id(2);
  double real_width = INITIAL_REAL_WIDTH * pow(ZOOM, frame);
  double imag_height = INITIAL_IMAG_HEIGHT * pow(ZOOM, frame);
  double min_r = CENTER_REAL - real_width/2.0;
  double max_i = CENTER_IMAG + imag_height/2.0;
  double cell_width = real_width/WIDTH;
  double cell_height = imag_height/HEIGHT;
  double3 final_color = 0.0;
  double sample_step = 1.0 / (MS + 1);
  
  for(int i=1; i<=MS; i++){{
  for(int j=1; j<=MS; j++){{
  
  double x0 = min_r + (col + i*sample_step)*cell_width;
  double y0 = max_i - (row + j*sample_step)*cell_height;
  double x = 0.0;
  double y = 0.0;
  double x2 = 0.0;
  double y2 = 0.0;
  bool broke = false;
  int iter;
  
  for(iter = 0; iter < MAX_ITERS; iter++){{
    y = (x + x)*y + y0;
    x = x2 - y2 + x0;
    x2 = x*x;
    y2 = y*y;
    if((x2 + y2) > 100.0) {{
      broke = true;
      break;
    }}
  }}
  if(broke) {{
    double smooth_iter_count = sqrt(iter + 1 - log2(log(x2 + y2)/2.0));
    int smooth_iter_floor = (int)smooth_iter_count;
    double3 color1 = palette[smooth_iter_floor % PALETTE_LEN];
    double3 color2 = palette[(1 + smooth_iter_floor) % PALETTE_LEN];
    final_color += mix(color1, color2, smooth_iter_count - smooth_iter_floor);
  }}
  }}
  }}
  
  if(any(final_color != 0.0)){{
    final_color = sqrt(final_color) / MS;
    int4 pos = (int4)(col, row, frame, 0);
    write_imageui(result, pos, (uint4)(final_color.x, final_color.y, final_color.z, 0.0));
  }}
}}
"""


def main():
    print(globals())
    # filename = 'mandelbrot-%04d.ppm'
    filename = 'zoom.mkv'
    ffmpeg_args = [
        'ffmpeg',
        '-f', 'rawvideo',
        '-video_size', f'{WIDTH}x{HEIGHT}',
        '-pixel_format', 'rgb24',
        '-framerate', '30',
        '-i', '-',
    ]
    ctx = cl.create_some_context()
    match ctx.devices[0].vendor:
        case 'Advanced Micro Devices, Inc.':
            ffmpeg_args.extend([
                '-c:v', 'hevc_amf',
                '-level', '4.1',
                '-quality', 'balanced',
                '-rc', 'cqp',
                '-qp_i', '27',
                '-qp_p', '27',
                filename
            ])
        case 'NVIDIA Corporation':
            ffmpeg_args.extend([
                '-c:v', 'hevc_nvenc',
                '-level', '4.1',
                '-preset', 'medium',
                '-rc', 'constqp',
                '-qp', '27',
                filename
            ])
        case _:
            # fall back to cpu encoding
            ffmpeg_args.extend([
                '-vcodec', 'libx265',
                '-crf', '27',
                filename
            ])

    print('Rendering on', ', '.join(d.name for d in ctx.devices))
    print('Using ffmpeg args:', ffmpeg_args)
    start = perf_counter()
    queue = cl.CommandQueue(ctx)
    program = cl.Program(ctx, KERNEL).build()

    palette_buf = cl.Buffer(ctx, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=PALETTE_LINEAR)
    result_format = cl.ImageFormat(cl.channel_order.RGBA, cl.channel_type.UNSIGNED_INT8)
    result_buf = cl.Image(ctx, cl.mem_flags.WRITE_ONLY, result_format, (WIDTH, HEIGHT, FRAMES))
    result = np.zeros((HEIGHT, WIDTH), dtype=cl.cltypes.uchar4)
    result.dtype = cl.cltypes.uchar

    program.mandelbrot(queue, (WIDTH, HEIGHT, FRAMES), None, palette_buf, result_buf)

    if os.path.isfile(filename):
        os.remove(filename)

    ffmpeg = subprocess.Popen(
        ffmpeg_args,
        stdin=subprocess.PIPE,
    )

    for i in range(FRAMES):
        cl.enqueue_copy(queue, result, result_buf, origin=(0, 0, i), region=(WIDTH, HEIGHT))
        result_final = np.ascontiguousarray(result.reshape(-1, 4)[:, :-1])
        ffmpeg.stdin.write(result_final)

    ffmpeg.stdin.flush()
    ffmpeg.stdin.close()
    ffmpeg.wait()

    if ffmpeg.returncode:
        print('ffmpeg failed.')
    print('Done in', perf_counter() - start, 'seconds.')


if __name__ == '__main__':
    main()
